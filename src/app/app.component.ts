import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import * as firebase from 'firebase';

const config = {
  apiKey: "AIzaSyDd720Hu0-xkXGCFr7aFCMzJ4zB_gKwXUE",
  authDomain: "backpacking-87e01.firebaseapp.com",
  databaseURL: "https://backpacking-87e01.firebaseio.com",
  projectId: "backpacking-87e01",
  storageBucket: "backpacking-87e01.appspot.com",
  messagingSenderId: "605407740439"
};

import { HomePage } from '../pages/home/home';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
    firebase.initializeApp(config);
  }
}

